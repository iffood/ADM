/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class ListaPedidos extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private ListView listaPendentes;
    private String parametros;
    private String URL;
    private ArrayList<String> items;
    private ArrayList<String> itemsPedidoComplemento;
    private JSONObject pedido;
    private String localString;
    private String IDUSER;
    private int x=3;
    private View view;
    private SharedPreferences.Editor editor;
    private String idpedido;
    private String user;
    private String senha;
    private ArrayList<String> IDLOC;
    private ArrayList<String> NOMELOC;
    private String NomeUser;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private BottomNavigationView navigation;
    private RecyclerView recyclerView;
    private Context context;
    public ArrayList<String> cliente;
    public ArrayList<String> localtxt;
    private ArrayList<String> formaPagamento;
    private ArrayList<String> itemsid;
    private JSONArray pedidos;
    private int ReloadLoc=0;
    private TextView pedidosAceitos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pedidos);
        progressBar = findViewById(R.id.progressLista);
        linearLayout = findViewById(R.id.layoutprogressBarLista);
        navigation =  findViewById(R.id.navigation);
        recyclerView = findViewById(R.id.recyclePedidosAceitos);
        pedidosAceitos = findViewById(R.id.pedidosaceitostxt);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().findItem(R.id.PedidosAceitos).setChecked(true);
        try{
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            NomeUser = sharedPreferences.getString("NOMEUSER","Nome Não Identificado");
            if(VerificaraConexao()){
                GetLocais();
                progressbar();
            }else{
                showSnackBar(ListaPedidos.this,"Nenhuma Conexão Detectada!");
            }
            try{
                Thread.sleep(300);
            }catch (Exception e){
                e.printStackTrace();
            }
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarListaP);
            toolbar.setTitle("IFFood");
            toolbar.setSubtitle(NomeUser);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            IDUSER = sharedPreferences.getString("IDUSER","");
            user = sharedPreferences.getString("User","");
            senha = sharedPreferences.getString("Pass","");
            if(VerificaraConexao()){
                URL = "http://iffood.info/_core/_controller/pedidos.php";
                Autenticação("getPedidosWhere");
                new ListaPedidos.solicitaDados().execute(URL);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        context = this;
    }
    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.Pedidos:
                    Intent intent = new Intent(ListaPedidos.this,pedidosActivity.class);
                    startActivity(intent);
                    finish();
                    return true;
                case R.id.PedidosAceitos:
                    return true;
                case R.id.ManipularProdutos:
                    Intent i2 = new Intent(ListaPedidos.this,Manipularprodutos.class);
                    startActivity(i2);
                    finish();
                    return true;
            }
            return false;
        }
    };
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try{
            if(item.getItemId()==R.id.RefeshMenu){
                Intent intent = new Intent(ListaPedidos.this,ListaPedidos.class);
                startActivity(intent);
                finish();
            }
            return super.onOptionsItemSelected(item);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menurefesh, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean VerificaraConexao(){
        try{
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            } else if(x==3) {
                x=0;
                Snackbar snackbar = Snackbar
                        .make(view, "Nenhuma Conexão Detectada!", Snackbar.LENGTH_LONG);
                snackbar.show();
                return false;
            }else{
                x=x+1;
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        }
    private void Autenticação(String action) {
        try{
            parametros = "action=" + action + "&status=" + "aceitado" + "&vendedor="+IDUSER;
        }catch (Exception e){
            e.printStackTrace();
        }
        }
    public void progressbar(){
        linearLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }
    public void TirarProgressBar(){
        linearLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }
    public void ReloadLocais() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (VerificarConexao()) {
                    GetLocais();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(ReloadLoc==10){
                        ReloadLoc=0;
                        Autenticação("getPedidosWhere");
                        Log.i("GETPEDIDIS","WHRE3");
                        new ListaPedidos.solicitaDados().execute(URL);
                    }else{
                        ReloadLoc++;
                    }
                }
            }
        }).start();


    }
    //Verifica a conexão
    public boolean VerificarConexao() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable()) {
                return true;
            }
            if (networkInfo == null) {
                return false;
            }
            showSnackBar(ListaPedidos.this, "Nenhuma Conexão Detectada!");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    private class solicitaDados extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... urls) {
        try{
            return Connection.postDados(urls[0], parametros);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    @Override
    protected void onPostExecute(String resposta) {
        try {
            if (VerificarConexao()) {
                if (resposta != null) {

                    Log.i("DUESPOSTA",resposta);
                    //Lista os items e guarda eles em Strings junto com nome,id,local,complemento.
                    cliente = new ArrayList<String>();
                    items = new ArrayList<String>();
                    localtxt = new ArrayList<String>();
                    itemsPedidoComplemento = new ArrayList<String>();
                    itemsid = new ArrayList<String>();
                    formaPagamento= new ArrayList<String>();
                    pedidos = new JSONArray(resposta);

                    for (int i = 0; i < pedidos.length(); i++) {
                        if (VerificarConexao()) {
                            pedido = pedidos.getJSONObject(i);
                            String name = pedido.getString("cliente");
                            String pagamento = pedido.getString("payment");
                            String troco = pedido.getString("troco");
                            String local = pedido.getString("local");
                            String complemento = pedido.getString("complemento");
                            String id = pedido.getString("id");
                            //Verifica os locais para mostrar para o vendedor em String
                            if (IDLOC.size() == 0 || IDLOC.isEmpty()) {
                                ReloadLocais();
                            } else {
                                TirarProgressBar();
                            }
                            for (int l = 0; l < IDLOC.size(); l++) {
                                if (local.equals(IDLOC.get(l))) {
                                    localString = NOMELOC.get(l);
                                    Log.d("LocaLNOME", localString);
                                    Log.d("LocaLID", IDLOC.get(l));
                                }
                            }
                                items.add("Cliente:" + name+"\n"+"Local De Entrega:" + localString);
                                cliente.add("Cliente:" + name);
                                localtxt.add("Local De Entrega:" + localString);

                            if (pedido.getString("complemento").contains("null")) {
                                itemsPedidoComplemento.add("Não possuí complemento");
                                formaPagamento.add("Forma de pagamento:" + pagamento + "\nTroco:" + "R$"+troco);
                            } else {
                                //Se tiver complemento vai ser add aqui
                                itemsPedidoComplemento.add("Complemento do local:" + complemento);
                                formaPagamento.add("Forma de pagamento:" + pagamento + "\nTroco:" + "R$"+troco);
                            }
                            //Adiciona o ID do pedido
                            itemsid.add(id);

                        }


                    }
                    if(pedidos.length()==0){
                        TirarProgressBar();
                        pedidosAceitos.setText("Sem pedidos no momento :(");
                    }
                    recyclerView.setAdapter(new AdapterPedidos(ListaPedidos.this,context,items,cliente,localtxt,itemsid,itemsPedidoComplemento,formaPagamento,true));
                }
            }
        } catch (org.json.JSONException e) {
            e.printStackTrace();
        }
    }
    }
    private void GetLocais(){
        IDLOC = new ArrayList<String>();
        NOMELOC = new ArrayList<String>();
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://iffood.info/_core/_controller/locais.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try{
                            final JSONArray pedidos = new JSONArray(response);
                            for (int i = 0; i < pedidos.length(); i++) {
                                pedido = pedidos.getJSONObject(i);
                                IDLOC.add(pedido.getString("id"));
                                NOMELOC.add(pedido.getString("nome"));
                                Log.d("TESTANDO",""+NOMELOC+"ID:"+IDLOC);
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response",""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("action", "getLocais");
                return params;
            }
        };
        queue.add(postRequest);
    }
}