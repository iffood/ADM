/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by lebar on 08/05/2018.
 */

public class AdapterProdutos extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
Context context;
ArrayList<String> items;
ArrayList<String> nomeProduto;
ArrayList<String> produtosDisponiveis;
ArrayList<String> preco;
ArrayList<String> id;
ArrayList<String> imglink;
Activity activity;

   public AdapterProdutos(Activity activity, Context context, ArrayList<String> items,
                          ArrayList<String> nomeProduto,
                          ArrayList<String> produtosDisponiveis,
                          ArrayList<String> preco,
                          ArrayList<String> id,
                          ArrayList<String> imglink){
   this.context = context;
   this.items = items;
   this.nomeProduto = nomeProduto;
   this.produtosDisponiveis = produtosDisponiveis;
   this.preco = preco;
   this.id = id;
   this.activity = activity;
   this.imglink = imglink;
       Log.i("PRODUT",context+""+items+""+nomeProduto+produtosDisponiveis+preco+id+imglink+"");
   }
   @Override
   public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View row = inflater.inflate(R.layout.costume_produtos,parent,false);
    Item item = new Item(row);
    return item;
   }

   @Override
   public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
    ((Item)holder).textView.setText(items.get(position));
    if(CheckInternet.isNetwork(context)==true){
        ((Item)holder).botEdit.setOnClickListener(new View.OnClickListener() {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            @Override
            public void onClick(View v) {
                if(CheckInternet.isNetwork(context)==true){
                    editor.putString("produto",nomeProduto.get(position));
                    editor.putString("Produtosdisponiveis",produtosDisponiveis.get(position));
                    editor.putString("preco",preco.get(position));
                    editor.putString("linkimg",imglink.get(position));
                    editor.putString("idProdutoManipular",id.get(position));
                    editor.apply();
                    Intent intent = new Intent(context,ManipularDadosProduto.class);
                    context.startActivity(intent);
                }else{
                    Toast.makeText(context, "Nenhuma Conexão Detectada!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }else{
        pedidosActivity pedidosActivity = new pedidosActivity();
        pedidosActivity.showSnackBar(activity,"Nenhuma Conexão Detectada!");
    }
   }

   @Override
   public int getItemCount() {
    return items.size();
   }
    public class Item extends RecyclerView.ViewHolder{
    TextView textView;
    Button botEdit;
     public Item(View itemView) {
      super(itemView);
      textView = itemView.findViewById(R.id.item);
      botEdit = itemView.findViewById(R.id.botEditProduto);
     }
    }

}

