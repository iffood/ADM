/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Manipularprodutos extends AppCompatActivity {

    private String URL;
    private String parametros;
    private ArrayList<String> Produtos;
    private ArrayList<String> nomeProdutos;
    private ArrayList<String> ProdutosDisponiveis;
    private ArrayList<String> preco;
    private ArrayList<String> idProduto;
    private ArrayList<String> imgLink;
    private JSONObject respostaOBJ;
    private JSONArray respostaArray;
    private ListView listView;
    private SharedPreferences.Editor editor;
    private BottomNavigationView navigation;
    private SharedPreferences sharedPreferences;
    private String NomeUser;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private RecyclerView recyclerView;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manipularprodutos);
        navigation =  findViewById(R.id.navigation);
        progressBar = findViewById(R.id.progressLista);
        linearLayout = findViewById(R.id.layoutprogressBarLista);
        recyclerView = findViewById(R.id.recycleProdutos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(Conexao()){
            progressbar();
        }else{
            showSnackBar(Manipularprodutos.this,"Nenhuma Conexão Detectada!");
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().findItem(R.id.ManipularProdutos).setChecked(true);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NomeUser = sharedPreferences.getString("NOMEUSER","Nome Não Identificado");

       // listView = findViewById(R.id.listaProdutos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarListaProdutos);
        toolbar.setTitle("IFFood");
        toolbar.setSubtitle(NomeUser);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        Conexao();
        if(Conexao()==true){
            URL = "http://iffood.info/_core/_controller/produtos.php";
            getProdutos("getAllProdutos");
            new Manipularprodutos.solicitaDados().execute(URL);
        }
        context = this;
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.Pedidos:
                    Intent intent = new Intent(Manipularprodutos.this,pedidosActivity.class);
                    startActivity(intent);
                    finish();
                    return true;
                case R.id.PedidosAceitos:
                    Intent i2 = new Intent(Manipularprodutos.this,ListaPedidos.class);
                    startActivity(i2);
                    finish();
                    return true;
                case R.id.ManipularProdutos:
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                Intent intent = new Intent(Manipularprodutos.this, pedidosActivity.class);
                startActivity(intent);
                finish();
            }
            if(id ==R.id.AddProduto){
                Intent intent = new Intent(Manipularprodutos.this,AdicionarProduto.class);
                startActivity(intent);
                finish();
            }
            return super.onOptionsItemSelected(item);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menumanipularprodutos, menu);
        return super.onCreateOptionsMenu(menu);
    }
    private boolean Conexao(){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }else{
                showSnackBar(Manipularprodutos.this,"Nenhuma Conexão Detectada!");
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }
    private void getProdutos(String action) {
        try{
            if(action.equals("getAllProdutos")){
                parametros = "action=" + action;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void progressbar(){
        linearLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }
    public void TirarProgressBar(){
        linearLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }
    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return Connection.postDados(urls[0], parametros);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(String resposta) {

            try{
                if(resposta!=null) {
                    Log.i("REQPOSTA",resposta);
                    nomeProdutos = new ArrayList<String>();
                    preco = new ArrayList<String>();
                    idProduto = new ArrayList<String>();
                    imgLink = new ArrayList<String>();
                    Produtos = new ArrayList<String>();
                    ProdutosDisponiveis = new ArrayList<String>();
                    respostaArray = new JSONArray(resposta);
                    for (int i = 0; i < respostaArray.length(); i++) {
                        respostaOBJ = respostaArray.getJSONObject(i);
                        String numeroDisponivel = respostaOBJ.getString("disponiveis");
                        int numeroDisponivelInt = Integer.parseInt(numeroDisponivel);
                        if(numeroDisponivelInt>1){
                            Produtos.add("Produto:"+respostaOBJ.getString("nome") + "\n" + "Disponíveis:" + respostaOBJ.getString("disponiveis")+" Unidades");
                        }else{
                            Produtos.add("Produto:"+respostaOBJ.getString("nome") + "\n" + "Disponíveis:" + respostaOBJ.getString("disponiveis")+" Unidade");
                        }
                        nomeProdutos.add(respostaOBJ.getString("nome"));
                        ProdutosDisponiveis.add(respostaOBJ.getString("disponiveis"));
                        preco.add(respostaOBJ.getString("preco"));
                        idProduto.add(respostaOBJ.getString("id"));
                        imgLink.add(respostaOBJ.getString("img"));
                        if(Produtos.size()==0||Produtos.isEmpty()){
                        }else{
                            TirarProgressBar();
                        }
                    }
                    if(respostaArray.length()==0){
                        TirarProgressBar();
                    }
                 //   ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(getApplicationContext(),
                   //         R.layout.mytextview, Produtos);
                  //  listView.setAdapter(mArrayAdapter);
                    recyclerView.setAdapter(new AdapterProdutos(Manipularprodutos.this,context,Produtos,nomeProdutos,ProdutosDisponiveis,preco,idProduto,imgLink));
                }
                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                editor = mPreferences.edit();
             /*   listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(Produtos!=null){
                        if(Conexao()==true){
                            editor.putString("produto",nomeProdutos.get(position));
                            editor.putString("Produtosdisponiveis",ProdutosDisponiveis.get(position));
                            editor.putString("preco",preco.get(position));
                            editor.putString("linkimg",imgLink.get(position));
                            editor.putString("idProdutoManipular",idProduto.get(position));
                            editor.apply();
                            Intent intent = new Intent(Manipularprodutos.this,ManipularDadosProduto.class);
                            startActivity(intent);
                        }
                    }
                    }
                });*/
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }
}

