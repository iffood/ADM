/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.security.AccessController.getContext;

/**
 * Created by lebar on 08/05/2018.
 */

public class AdapterPedidos extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<String> items;
    ArrayList<String> cliente;
    ArrayList<String> local;
    ArrayList<String> itemsid;
    ArrayList<String> itemsComplemento;
    ArrayList<String> formaPagamento;
    Activity activity;
    Button close;
    private ProgressBar barraCarregamento;
    TextView clientetxt;
    TextView localtxt;
    TextView complementotxt;
    TextView pagamentotxt;
    TextView rejeitarpedido;
    public BottomSheetDialog dialog;
    private JSONObject pedido;
    private JSONArray pedidos;
    private ArrayList<String> Produtos;
    private ArrayList<String> Produtos1;
    private ArrayList<String> Quantidade;
    private ArrayList<String> IDPROD;
    private ArrayList<String> NOMEPROD;
    private String parametros;
    private String User;
    private String Pass;
    private SharedPreferences sharedPreferences;
    private String URL;
    private TextView MaisinfoTrufas;
    View view;
    View viewPedidoAceito;
    private ScrollView scrollView;
    private Button aceitarP;
    boolean pedidosAceitos;



    public AdapterPedidos(Activity activity,
                          Context context,
                          ArrayList<String>items,
                          ArrayList<String> cliente,
                          ArrayList<String>local,
                          ArrayList<String> itemsid,
                          ArrayList<String> itemsComplemento,
                          ArrayList<String> formaPagamento,
                            boolean PedidosAceitos ){

            this.formaPagamento = formaPagamento;
            this.items = items;
            this.activity = activity;
            this.context = context;
            this.cliente = cliente;
            this.local = local;
            this.itemsid = itemsid;
            this.itemsComplemento = itemsComplemento;
            this.pedidosAceitos = PedidosAceitos;
            Log.i("ADAPTER",formaPagamento+""+items+activity+context+cliente+local+itemsid+itemsComplemento+"");
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.costume_pedidos,parent,false);
        Item item = new Item(row);
        return item;
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((Item)holder).textView.setText(items.get(position));
        ((Item)holder).button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onClick(View v) {
                Log.i("ITEN",items.get(position)+itemsid.get(position));
                if (CheckInternet.isNetwork(context)) {
                    //Verifica se o pedido foi aceito para abrir uma dialog diferente se ele não foi aceito
                    if(pedidosAceitos==true){
                        viewPedidoAceito = ((FragmentActivity)context).getLayoutInflater().inflate(R.layout.bottom_sheet_pedidosaceitos, null);
                        aceitarP = viewPedidoAceito.findViewById(R.id.confirmarpedido);
                        barraCarregamento =viewPedidoAceito.findViewById(R.id.progressbottom);
                        clientetxt = viewPedidoAceito.findViewById(R.id.clientetxt);
                        localtxt = viewPedidoAceito.findViewById(R.id.localtxt);
                        close = viewPedidoAceito.findViewById(R.id.close_bottom);
                        scrollView = viewPedidoAceito.findViewById(R.id.Scrollinfo);
                        rejeitarpedido = viewPedidoAceito.findViewById(R.id.recusarpedido);
                        complementotxt = viewPedidoAceito.findViewById(R.id.complementotxt);
                        pagamentotxt = viewPedidoAceito.findViewById(R.id.pagamentotxt);
                        MaisinfoTrufas = viewPedidoAceito.findViewById(R.id.ListarTrufas);
                        dialog = new BottomSheetDialog(context);
                        aceitarP.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(CheckInternet.isNetwork(context)==true){
                                    ConfirmarPedidoParametro("confirmarPedido",itemsid.get(position));
                                    new AdapterPedidos.solicitaDados().execute(URL);
                                }else{
                                    Toast.makeText(context, "Nenhuma Conexão Detectada!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }else{
                        //se não foi aceito abre uma dialog diferente
                        view = ((FragmentActivity)context).getLayoutInflater().inflate(R.layout.bottom_sheet, null);
                        aceitarP = view.findViewById(R.id.aceitarPedido);
                        barraCarregamento =view.findViewById(R.id.progressbottom);
                        clientetxt = view.findViewById(R.id.clientetxt);
                        localtxt = view.findViewById(R.id.localtxt);
                        close = view.findViewById(R.id.close_bottom);
                        scrollView = view.findViewById(R.id.Scrollinfo);
                        rejeitarpedido = view.findViewById(R.id.recusarpedido);
                        complementotxt = view.findViewById(R.id.complementotxt);
                        pagamentotxt = view.findViewById(R.id.pagamentotxt);
                        MaisinfoTrufas = view.findViewById(R.id.ListarTrufas);
                        dialog = new BottomSheetDialog(context);
                        aceitarP.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(CheckInternet.isNetwork(context)==true){
                                    ConfirmarPedidoParametro("aceitarPedido",itemsid.get(position));
                                    new AdapterPedidos.solicitaDados().execute(URL);
                                }else{
                                    Toast.makeText(context, "Nenhuma Conexão Detectada!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    //Rejeita o pedido e vale para ambos casos caso o pedido seja aceito ou não
                    rejeitarpedido.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CustomDialogClass cdd=new CustomDialogClass(activity,itemsid.get(position),context);
                            cdd.setCancelable(false);
                            cdd.show();
                        }
                    });
                    //Seta uma prioridade paraa o scroll view dos pedidos====
                    scrollView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            scrollView.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;
                        }
                    });
                    //========
                    //só setando textos
                localtxt.setText(local.get(position));
                clientetxt.setText(cliente.get(position));
                complementotxt.setText(itemsComplemento.get(position));
                pagamentotxt.setText(formaPagamento.get(position));
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                User = sharedPreferences.getString("User","");
                Pass = sharedPreferences.getString("Pass","");
                if(CheckInternet.isNetwork(context)==true){
                        URL = "http://iffood.info/_core/_controller/pedidos.php";
                        GetProdutos();
                    try{
                        Thread.sleep(300);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                    ConfirmarPedidoParametro("getItemsOfPedido",itemsid.get(position));
                    new AdapterPedidos.solicitaDados().execute(URL);
                }else{
                    Toast.makeText(context, "Nenhuma Conexão Detectada!", Toast.LENGTH_SHORT).show();
                }
                //botão de close da dialog representado pelo "X"
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                //Verifica mais uma vez qual dialog abrir
                if(pedidosAceitos==true){
                    dialog.setContentView(viewPedidoAceito);
                    dialog.show();
                }else{
                    dialog.setContentView(view);
                    dialog.show();
                }

                }else{
                    showSnackBar(activity,"Nenhuma Conexão Detectada!");
                }
            }
        });
    }
    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }
    private void ConfirmarPedidoParametro(String action,String IDCONCRETO) {
        try {
            if (action.equals("aceitarPedido")) {
                parametros = "action=" + action + "&pedido=" + IDCONCRETO + "&user_vendedor=" + User + "&pass_vendedor=" + Pass;
                Log.i("Verifica", User + ":" + Pass + IDCONCRETO);
            } else if (action.equals("getItemsOfPedido")) {
                parametros = "action=" + action + "&id=" + IDCONCRETO;
            }else if(action.equals("confirmarPedido")){
                parametros = "action=" + action + "&id=" + IDCONCRETO + "&user_vendedor=" + User + "&pass_vendedor=" + Pass;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void GetProdutos(){
        IDPROD = new ArrayList<String>();
        NOMEPROD = new ArrayList<String>();
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://iffood.info/_core/_controller/produtos.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try{
                            final JSONArray pedidos = new JSONArray(response);
                            for (int i = 0; i < pedidos.length(); i++) {
                                pedido = pedidos.getJSONObject(i);
                                IDPROD.add(pedido.getString("id"));
                                NOMEPROD.add(pedido.getString("nome"));
                                Log.d("PRODPROD",""+IDPROD+":"+NOMEPROD);
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response",""+error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("action", "getProdutos");
                return params;
            }
        };
        queue.add(postRequest);
    }
    public void Progressbar (){
        barraCarregamento.setVisibility(View.VISIBLE);
    }
    public void DimissProgress(){
        barraCarregamento.setVisibility(View.GONE);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }
    public class Item extends RecyclerView.ViewHolder{
        TextView textView;
        Button button;

        public Item(View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.botMaisinfo);
            textView = itemView.findViewById(R.id.item);
        }
    }

    class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return Connection.postDados(urls[0], parametros);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(String resposta) {
            Log.i("REQPOSTAConfirma",resposta);
            try{
                if(resposta.contains("true")){
                    pedidosActivity pedidosActivity = new pedidosActivity();
                    pedidosActivity.ReloadLocais();
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                    pedidosActivity.showSnackBar(activity,"Tudo Certo!");
                }
                Produtos = new ArrayList<String>();
                Quantidade = new ArrayList<String>();
                Produtos1 = new ArrayList<String>();
                pedidos = new JSONArray(resposta);
                for (int i = 0;i<pedidos.length(); i++ ) {
                    pedido = pedidos.getJSONObject(i);
                    Produtos.add(pedido.getString("produto"));
                    Log.i("IDPROD",""+Produtos);
                    for(int p = 0;p<IDPROD.size();p++){
                        if(Produtos.get(i).equals(IDPROD.get(p))) {
                            Produtos1.add(NOMEPROD.get(p));
                            Log.i("PRODUTOS1",Produtos1+"");
                        }
                    }

                    Quantidade.add(pedido.getString("quantidade"));
                    if(Quantidade.size()==0 ||Quantidade.isEmpty()){
                        Progressbar();
                    }else{
                        DimissProgress();
                    }
                    if(IDPROD.size()==0||IDPROD.isEmpty()){
                        Progressbar();
                    }else{
                        DimissProgress();
                    }
                }
                for(int i =0;i<Produtos1.size();i++){
                    Log.i("testando",Produtos1.get(i)+"::::"+Quantidade.get(i));
                    if(Produtos1.size()>1){
                        MaisinfoTrufas.append("\nProduto:"+Produtos1.get(i)+"\nQuantidade:"+Quantidade.get(i)+"\n");
                    }else{
                        MaisinfoTrufas.append("Produto:"+Produtos1.get(i)+"\nQuantidade:"+Quantidade.get(i));
                    }

                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

}
class CheckInternet {

    public static boolean isNetwork(Context context) {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    }

