/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class pedidosActivity extends AppCompatActivity {
    boolean verify = true;
    //cria o handler e define o delay do bagui
    Handler h = new Handler();
    Handler min = new Handler();
    int delaymin = 60 * 1000;
    Runnable runnablemin;
    int delay = 15 * 1000; //1 second=1000 milisecond, 15*1000=15seconds
    Runnable runnable;
    private AlertDialog alerta;
    private String URL;
    private String URLVendedor;
    private String parametros;
    private ListView listaDePeididos;
    private String localString;
    private JSONObject pedido;
    private String idpedido;
    public ArrayList<String> items;
    public ArrayList<String> cliente;
    public ArrayList<String> localtxt;
    private ArrayList<String> IDLOC;
    private ArrayList<String> NOMELOC;
    private ArrayList<String> itemsid;
    private JSONArray pedidos;
    private ArrayList<String> itemsPedidoComplemento;
    private ArrayList<String> formaPagamento;
    private SharedPreferences.Editor editor;
    private SharedPreferences sharedPreferences;
    private View view;
    private boolean VerificarConnection = false;
    private int y = 0;
    private String user;
    private String senha;
    private int test = 0;
    private boolean HabilitarNotB;
    private String NomeUser;
    private int VerifiN = 1;
    public NotificationManager nm;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private Toolbar toolbar;
    private int ReloadLoc=0;
    private TextView pedidosAguardo;
    private BottomNavigationView navigation;

    private RecyclerView recyclerView;
 private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HabilitarNotB = true;
        setContentView(R.layout.activity_pedidos);
                view = findViewById(R.id.view);
                progressBar = findViewById(R.id.progress);
                linearLayout = findViewById(R.id.layoutprogressBar);
                pedidosAguardo = findViewById(R.id.PedidosEmAguardotxt);
                navigation = findViewById(R.id.navigation);
                recyclerView = findViewById(R.id.recycle);
                navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
                navigation.getMenu().findItem(R.id.Pedidos).setChecked(true);
                startService(new Intent(getBaseContext(), OnClearFromRecentService.class));
                recyclerView.setLayoutManager(new LinearLayoutManager(this));

                try {
                    SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    editor = Preferences.edit();
                    editor.putBoolean("PING", true).apply();
                    Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    NomeUser = sharedPreferences.getString("NOMEUSER", "Nome Não Identificado");
                    VerificarConexao();
                    if (VerificarConexao()) {
                        GetLocais();
                        progressbar();
                    }else{
                        showSnackBar(pedidosActivity.this,"Nenhuma Conexão Detectada!");
                    }
                    toolbar = (Toolbar) findViewById(R.id.toolbarPedidos);
                    toolbar.setTitle("IFFood");
                    toolbar.setSubtitle(NomeUser);
                    setSupportActionBar(toolbar);
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    user = sharedPreferences.getString("User", "");
                    senha = sharedPreferences.getString("Pass", "");

                    if (VerificarConexao()) {
                        //Se verificaConexão==true ele seta  url e manda pedir os pedidos
                        URL = "http://iffood.info/_core/_controller/pedidos.php";
                        URLVendedor = "http://iffood.info/_core/_controller/vendedores.php";
                        GetPedidos("getPedidosWhere");
                        Log.i("GETPEDIDIS", "WHRE1");
                        new solicitaDados().execute(URL);
                        wait(300);
                         GetPedidos("ping");
                        new solicitaDados().execute(URLVendedor);
                        Log.i("Entrou", "Entrou");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                context = this;
            }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.Pedidos:
                    return true;
                case R.id.PedidosAceitos:
                    Intent i = new Intent(pedidosActivity.this,ListaPedidos.class);
                    startActivity(i);
                    finish();
                    return true;
                case R.id.ManipularProdutos:
                    Intent i2 = new Intent(pedidosActivity.this,Manipularprodutos.class);
                    startActivity(i2);
                    finish();
                    return true;
            }
            return false;
        }
    };

    public void ReloadPAGE() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (VerificarConexao()) {
                    GetLocais();
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    GetPedidos("getPedidosWhere");
                    Log.i("GETPEDIDIS","WHRE2");
                    new solicitaDados().execute(URL);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(pedidosActivity.this, pedidosActivity.class);
                    startActivity(intent);

                } else {
                    showSnackBar(pedidosActivity.this, "Erro na conexão!");
                }
            }
        }).start();
    }

    public void ReloadLocais() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (VerificarConexao()) {
                    GetLocais();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(ReloadLoc==10){
                        ReloadLoc=0;
                        GetPedidos("getPedidosWhere");
                        Log.i("GETPEDIDIS","WHRE3");
                        new solicitaDados().execute(URL);
                    }else{
                        ReloadLoc++;
                    }
                }
            }
        }).start();


    }

    //Metodo para as notificações
    public void Notifications(View view) {
        try {
            if (!HabilitarNotB) {
                Intent intent = new Intent(this, pedidosActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_shopping_cart_black_24dp)
                                .setContentTitle("IFFood pedidos")
                                .setContentText("IFFood possuí novos pedidos para serem entregues")
                                .setDefaults(Notification.DEFAULT_ALL) // requires VIBRATE permission
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true)
                                .setColor(getResources().getColor(R.color.colorAccent))
                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.if_ic))
                                .setStyle(new NotificationCompat.BigTextStyle().bigText("IFFood possuí novos pedidos para serem entregues"));
                nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(0, builder.build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void NOTFICACAOPING(View view) {
        try {
            Intent intent = new Intent(this, pedidosActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_signal_wifi_notification_black_24dp)
                            .setContentTitle("IFFood")
                            .setContentText("Sua conexão foi perdida,você está offline agora!")
                            .setDefaults(Notification.DEFAULT_LIGHTS) // requires VIBRATE permission
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true)
                            .setColor(getResources().getColor(R.color.colorAccent))
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.if_ic))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText("Sua conexão foi perdida,você está offline agora!"));
            nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nm.notify(0, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ////////////////////////////////////
    //PRECISO TERMINAR ISSO AINDA
    public void NOTFICACAOPINGExecutando(View view) {
        try {
                Intent intent = new Intent(this, pedidosActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.ic_shopping_cart_black_24dp)
                                .setContentTitle("IFFood")
                                .setContentText("Você ainda está online")
                                .setDefaults(Notification.DEFAULT_LIGHTS) // requires VIBRATE permission
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true)
                                .setColor(getResources().getColor(R.color.colorAccent))
                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.if_ic))
                                .setStyle(new NotificationCompat.BigTextStyle().bigText("Você ainda está online para o servidor!"));
                nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(123, builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //metodo para mostrar a snackbar
    @SuppressLint("WrongConstant")
    public void showSnackBar(Activity activity, String message) {
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        snackbar.show();
        TextView txtv =  view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            txtv.setTextAlignment(Gravity.CENTER_HORIZONTAL);
        }
    }

    //Verifica a conexão
    public boolean VerificarConexao() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable()) {
                return true;
            }
            if (networkInfo == null) {
                return false;
            }
            showSnackBar(pedidosActivity.this, "Nenhuma Conexão Detectada!");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onResume() {
        //start handler as activity become visible
        min.postDelayed(new Runnable() {
            public void run() {
                try {
                    //Roda alguma coisa de tempo em tempo e verifica a conexão de tempos em tempos
                    if (VerificarConexao()) {
                    /*
                    Aqui é se a conexão estiver normal o y=1
                    se ela cair o y=0 e o verificarConnection recebe true
                    e quando a conexão voltar o verificarConnection recebe false pra não ficar entrando infinitamente no if
                    e restarta a activity por causa de um bug
                    se não restartar a activity o app fecha
                     */
                        SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        boolean verify = Preferences.getBoolean("PING", true);
                        if (verify) {
                            y = 1;
                            if (VerificarConnection) {
                                if (y == 1) {
                                }
                            } else {
                                GetPedidos("ping");
                                new solicitaDados().execute(URLVendedor);
                                Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                                Log.i("ENTROUPING", "PING");
                            }
                        }
                    } else {
                        NOTFICACAOPING(view);
                        VerificarConnection = true;
                        y = 0;
                    }
                    //do something
                    runnable = this;
                    h.postDelayed(runnable, delaymin);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, delay);
        super.onResume();

        h.postDelayed(new Runnable() {
            public void run() {
                try {
                    //Roda alguma coisa de tempo em tempo e verifica a conexão de tempos em tempos
                    if (VerificarConexao()) {
                    /*
                    Aqui é se a conexão estiver normal o y=1
                    se ela cair o y=0 e o verificarConnection recebe true
                    e quando a conexão voltar o verificarConnection recebe false pra não ficar entrando infinitamente no if
                    e restarta a activity por causa de um bug
                    se não restartar a activity o app fecha
                     */

                        y = 1;
                        if (VerificarConnection) {
                            if (y == 1) {
                                Log.i("Verificar", "verifica");
                                VerificarConnection = false;
                                ReloadPAGE();
                                NOTFICACAOPINGExecutando(view);

                            }
                        } else {
                            GetPedidos("getPedidosWhere");
                            Log.i("GETPEDIDIS","WHRE4");
                            new solicitaDados().execute(URL);
                        }
                    } else {
                        NOTFICACAOPING(view);
                        VerificarConnection = true;
                        y = 0;
                        // showSnackBar(pedidosActivity.this,"Nenhuma Conexão Detectada!");

                    }
                    //do something
                    runnable = this;
                    h.postDelayed(runnable, delay);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, delay);
        super.onResume();
    }

    @Override
    protected void onPause() {
        //callback para para não consumir memoria e internet do vendedor
        h.removeCallbacks(runnable); //stop handler when activity not visible
        SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean verify = Preferences.getBoolean("PING", true);
        if (verify) {
            if (VerificarConexao()) {
                NOTFICACAOPINGExecutando(view);
            }
            min.postDelayed(new Runnable() {
                public void run() {
                    try {
                        //Roda alguma coisa de tempo em tempo e verifica a conexão de tempos em tempos
                        if (VerificarConexao()) {
                    /*
                    Aqui é se a conexão estiver normal o y=1
                    se ela cair o y=0 e o verificarConnection recebe true
                    e quando a conexão voltar o verificarConnection recebe false pra não ficar entrando infinitamente no if
                    e restarta a activity por causa de um bug
                    se não restartar a activity o app fecha
                     */
                            SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            boolean verify = Preferences.getBoolean("PING", true);
                            if (verify) {
                                GetPedidos("ping");
                                new solicitaDados().execute(URLVendedor);
                                Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                                Log.i("ENTROUPING", "PING");

                            } else {
                                Log.i("NãoENTRoPING", "PING");
                                Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                            }

                        } else {
                            NOTFICACAOPING(view);
                        }
                        //do something
                        runnable = this;
                        h.postDelayed(runnable, delaymin);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, delay);
            super.onResume();
        } else {
            nm.cancel(0);
            min.removeCallbacks(runnablemin);
            Log.i("DESTROY", "SAIU VIADO");
        }

        super.onPause();
    }

    @Override
    protected void onStop() {
        //callback para para não consumir memoria e internet do vendedor
        h.removeCallbacks(runnable);
        min.removeCallbacks(runnablemin);
        Log.i("DESTROY", "SAIU VIADO");
        super.onStop();
    }

    @Override
    protected void onPostResume() {
        //callback para para não consumir memoria e internet do vendedor
        SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean verify = Preferences.getBoolean("PING", true);
        if (verify) {
            if (VerificarConexao()) {
                NOTFICACAOPINGExecutando(view);
            }
            min.postDelayed(new Runnable() {
                public void run() {
                    try {
                        //Roda alguma coisa de tempo em tempo e verifica a conexão de tempos em tempos
                        if (VerificarConexao()) {
                    /*
                    Aqui é se a conexão estiver normal o y=1
                    se ela cair o y=0 e o verificarConnection recebe true
                    e quando a conexão voltar o verificarConnection recebe false pra não ficar entrando infinitamente no if
                    e restarta a activity por causa de um bug
                    se não restartar a activity o app fecha
                     */
                            SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            boolean verify = Preferences.getBoolean("PING", true);
                            if (verify) {
                             //   GetPedidos("ping");
                             //   new solicitaDados().execute(URLVendedor);
                                Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                                Log.i("ENTROUPING", "PING");

                            } else {
                                Log.i("NãoENTRoPING", "PING");
                                Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                            }
                        } else {
                            NOTFICACAOPING(view);
                        }
                        //do something
                        runnable = this;
                        h.postDelayed(runnable, delaymin);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, delay);
            super.onResume();
        } else {
            nm.cancel(0);
            min.removeCallbacks(runnablemin);
            Log.i("DESTROY", "SAIU VIADO");
        }
        super.onPostResume();
    }

    public void progressbar() {
        linearLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void TirarProgressBar() {
        linearLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        //callback para para não consumir memoria e internet do vendedor
        finish();
        h.removeCallbacks(runnable);
        min.removeCallbacks(runnablemin);
        super.onDestroy();
    }

    private void GetPedidos(String action) {
        try {
            //Envia os parametros e vê qual action é enviada
            if (action.equals("getPedidosWhere")) {
                parametros = "action=" + action + "&status=" + "aguardando";
            } else if (action.equals("aceitarPedido")) {
                parametros = "action=" + "aceitarPedido" + "&pedido=" + idpedido + "&user_vendedor=" + user + "&pass_vendedor=" + senha;
                Log.i("IdPed", idpedido);
            } else if (action.equals("getItemsOfPedido")) {
                parametros = "action=" + "getItemsOfPedido" + "&id=" + idpedido;
            } else if (action.equals("ping")) {
                parametros = "action=" + action + "&user=" + user + "&pass=" + senha;
            } else if (action.equals("getLocais")) {
                parametros = "action=" + action;
            }
            Log.i("TESTE PARAMETROS", parametros);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            //verifica se as notificações estão habilitadas se sim ele escolhe um menu se não ele escolhe o outro menu
            SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            editor = mPreferences.edit();
            boolean VerificarNot = mPreferences.getBoolean("Desabilitar", false);
            ///////////////////////////////////////////
            if (VerificarNot) {
                getMenuInflater().inflate(R.menu.menudesabilitado, menu);
            } else if (!VerificarNot) {
                getMenuInflater().inflate(R.menu.menu, menu);
            }
            return super.onCreateOptionsMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            int id = item.getItemId();
            if (id == R.id.logout) {
      //Se o usuario quiser deslogar ele verifica se ele quer mesmo deslogar e logo depois ele volta pra loginActivity
                //Cria o gerador do AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                //define o titulo
                builder.setTitle("Logout");
                //define a mensagem
                builder.setMessage("Tem certeza que deseja deslogar?");
                //define um botão como positivo
                builder.setPositiveButton("Deslogar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        editor = mPreferences.edit();
                        editor.remove("NOMEUSER").apply();
                        editor.remove("verificarLogin").apply();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                        Log.i("Verifica", "" + mPreferences.getBoolean("verificarLogin", false));
                    }
                });
                //define um botão como negativo.
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                    }
                });
                //cria o AlertDialog
                alerta = builder.create();
                //Exibe
                alerta.show();
            }
            if (id == R.id.ping) {
                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                verify = mPreferences.getBoolean("PING", false);
                if (verify) {
                    SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    editor = Preferences.edit();
                    editor.putBoolean("PING", false).apply();
                    item.setIcon(R.drawable.ic_wifi_red_24dp);
                    nm.cancel(0);
                    Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                    Log.i("COLOCOFALSE", "FALSE");
                }
                if (!verify) {
                    SharedPreferences Preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    editor = Preferences.edit();
                    editor.putBoolean("PING", true).apply();
                    item.setIcon(R.drawable.ic_wifi_black_24dp);
                    NOTFICACAOPINGExecutando(view);
                    Log.i("PINGOU", Preferences.getBoolean("PING", Boolean.parseBoolean(null)) + "");
                    Log.i("COLOCOTRUE", "TRUE");
                }

            }
            if (id == R.id.desabilitarNotificações) {
                Toast.makeText(this, "Notificações desabilitadas", Toast.LENGTH_LONG).show();
                //Desabilita as notificações enviando um SharedPreferences para guardar que o desabilitamento das notificações
                Intent intent = new Intent(this, pedidosActivity.class);
                startActivity(intent);
                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                editor = mPreferences.edit();
                editor.putBoolean("Desabilitar", true).apply();
                finish();

            }
            if (id == R.id.habilitarNotificações) {
                Toast.makeText(this, "Notificações habilitadas", Toast.LENGTH_LONG).show();
                //habilitada as notificações enviando um SharedPreferences para guardar que o habilitamento das notificações
                Intent intent = new Intent(this, pedidosActivity.class);
                startActivity(intent);
                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                editor = mPreferences.edit();
                mPreferences.edit().putBoolean("Desabilitar", false).apply();
                finish();
            }
            if (id == R.id.Refesh) {
                if(VerificarConexao()==true){
                        ReloadPAGE();
                        finish();
                }else{
                    showSnackBar(pedidosActivity.this,"Sem Conexão Com A Internet");
                }
            }
            if (id == R.id.Refesh1) {
                if(VerificarConexao()==true){
                        ReloadPAGE();
                        finish();
                }else{
                    showSnackBar(pedidosActivity.this,"Sem Conexão Com A Internet");
                }
            }
            return super.onOptionsItemSelected(item);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void GetLocais() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    IDLOC = new ArrayList<String>();
                    NOMELOC = new ArrayList<String>();
                    RequestQueue queue = Volley.newRequestQueue(pedidosActivity.this);
                    String url = "http://iffood.info/_core/_controller/locais.php";
                    StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    Log.d("Response", response);
                                    try {
                                        final JSONArray pedidos = new JSONArray(response);
                                        for (int i = 0; i < pedidos.length(); i++) {
                                            pedido = pedidos.getJSONObject(i);
                                            IDLOC.add(pedido.getString("id"));
                                            NOMELOC.add(pedido.getString("nome"));
                                            Log.d("TESTANDO", "" + NOMELOC + "ID:" + IDLOC);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "" + error);
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("action", "getLocais");
                            return params;
                        }
                    };
                    queue.add(postRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public class solicitaDados extends AsyncTask<String, Void, String> {
        //Solicita para o envio de dados
        @Override
        protected String doInBackground(String... urls) {
            return Connection.postDados(urls[0], parametros);
        }

        @Override
        protected void onPostExecute(String resposta) {
            try {
                if (VerificarConexao()) {
                    if (resposta != null) {

                        Log.i("RESPP",resposta);
                        //Lista os items e guarda eles em Strings junto com nome,id,local,complemento.
                        cliente = new ArrayList<String>();
                        items = new ArrayList<String>();
                        localtxt = new ArrayList<String>();
                        itemsPedidoComplemento = new ArrayList<String>();
                        itemsid = new ArrayList<String>();
                        formaPagamento= new ArrayList<String>();
                        pedidos = new JSONArray(resposta);

                            for (int i = 0; i < pedidos.length(); i++) {
                                if (VerificarConexao()) {
                                    pedido = pedidos.getJSONObject(i);
                                    String name = pedido.getString("cliente");
                                    String pagamento = pedido.getString("payment");
                                    String troco = pedido.getString("troco");
                                    String local = pedido.getString("local");
                                    String complemento = pedido.getString("complemento");
                                    String id = pedido.getString("id");
                                    //Verifica os locais para mostrar para o vendedor em String
                                    if (IDLOC.size() == 0 || IDLOC.isEmpty()) {
                                        ReloadLocais();
                                    } else {
                                        TirarProgressBar();
                                    }
                                    for (int l = 0; l < IDLOC.size(); l++) {
                                        if (local.equals(IDLOC.get(l))) {
                                            localString = NOMELOC.get(l);
                                            Log.d("LocaLNOME", localString);
                                            Log.d("LocaLID", IDLOC.get(l));
                                        }
                                    }

                                    if (pedido.getString("status").contains("aguardando")) {
                                            items.add("Cliente:" + name+"\n"+"Local De Entrega:" + localString);
                                           cliente.add("Cliente:" + name);
                                           localtxt.add("Local De Entrega:" + localString);
                                    }

                                    if (pedido.getString("complemento").contains("null")) {
                                        itemsPedidoComplemento.add("Não possuí complemento");
                                        formaPagamento.add("Forma de pagamento:" + pagamento + "\nTroco:" + "R$"+troco);
                                    } else {
                                        //Se tiver complemento vai ser add aqui
                                        itemsPedidoComplemento.add("Complemento do local:" + complemento);
                                        formaPagamento.add("Forma de pagamento:" + pagamento + "\nTroco:" + "R$"+troco);
                                    }
                                    //Adiciona o ID do pedido
                                    itemsid.add(id);

                                }


                            }


                            if(pedidos.length()==0){
                                TirarProgressBar();
                                pedidosAguardo.setText("Sem pedidos no momento :(");
                            }
                        if (VerifiN == 1) {
                            test = cliente.size();
                            VerifiN = 0;
                        }

                        //Lista Os Pedidos
                     //   ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<String>(getApplicationContext(),
                     //           R.layout.mytextview, items);
                      //  listaDePeididos.setAdapter(mArrayAdapter);
                       recyclerView.setAdapter(new AdapterPedidos(pedidosActivity.this,context,items,cliente,localtxt,itemsid,itemsPedidoComplemento,formaPagamento,false));
                    }
                }

                SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                editor = mPreferences.edit();
                boolean VerificarNot = mPreferences.getBoolean("Desabilitar", false);
                if (VerificarNot) {
                    if(cliente != null) {
                        test = cliente.size();
                    }
                    //Verificar Notifição True == não aparece notificação
                } else {
                    if (cliente != null){
                        if (cliente.size() > test) {
                            HabilitarNotB = false;
                            if (HabilitarNotB == false) {
                                //Aqui aparece a notificação
                                Notifications(view);
                                test = cliente.size();
                            }
                        }
                }
                }

                //ClickListener para a lista dos pedidos
            /*   listaDePeididos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                        try {
                            if (!VerificarConexao()) {
                                showSnackBar(pedidosActivity.this, "Nenhuma Conexão Detectada!");
                            }
                            if (itemsPedidoComplemento.get(position).equals("")) {
                                editor.putString("items", items.get(position));
                                editor.apply();
                            } else {
                                editor.putString("items", items.get(position));
                                editor.putString("itemsPedidoComplemento", itemsPedidoComplemento.get(position));
                                editor.apply();
                            }
                            idpedido = itemsid.get(position);
                            if (VerificarConexao()) {
                                //se tiver conexão vai enviar os parâmetros abrindo a activity de confirmar pedido e envia o id do pedido para que o mesmo seja confirmado e finaliza
                                //essa activity.
                                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                editor = sharedPreferences.edit();
                                Intent intentConfirmar = new Intent(getApplicationContext(), ConfirmarPedido.class);
                                editor.putString("idProduto", idpedido);
                                //=============================================================
                                editor.apply();
                                startActivity(intentConfirmar);
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });*/
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }
        }
    }
}