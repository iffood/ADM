/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by lebar on 14/05/2018.
 */

public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public TextView rejeitar;
    private Button cancelar;
    String id;
    String textoRejeitar;
    EditText texto;
    private String parametros;
    private String URL;
    private String User;
    private String Pass;
    private SharedPreferences sharedPreferences;
    Context context;
    int maxLength = 255;

    public CustomDialogClass(Activity a,String id,Context context) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.id = id;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        rejeitar = findViewById(R.id.btn_rejeitar);
        cancelar = findViewById(R.id.btn_cancelar);
        texto = findViewById(R.id.textoJustificativa);
        //FALTA SETAR O TAMANHO DA JUSTIFICATIVA
        texto.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
        rejeitar.setOnClickListener(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        User = sharedPreferences.getString("User","");
        Pass = sharedPreferences.getString("Pass","");


        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_rejeitar:
                textoRejeitar = texto.getText().toString();
                if(textoRejeitar.isEmpty()){
                    Toast.makeText(context, "Por favor,digite alguma justificativa.", Toast.LENGTH_SHORT).show();
                }else{
                    if(CheckInternet.isNetwork(c)){
                        URL = "http://iffood.info/_core/_controller/pedidos.php";
                        ConfirmarPedidoParametro("setObservacao",id);
                        new CustomDialogClass.solicitaDados().execute(URL);
                        try {
                            Thread.sleep(800);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ConfirmarPedidoParametro("rejeitarPedido",id);
                        new CustomDialogClass.solicitaDados().execute(URL);
                        //chamar o método por aqui
                    }else{
                        Toast.makeText(c, "Nenhuma Conexão Detectada!", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                break;
        }
    }
    private void ConfirmarPedidoParametro(String action,String IDCONCRETO) {
        try{
             if(action.equals("setObservacao")){
                parametros = "action="+action +"&id="+IDCONCRETO+"&user="+User+"&pass="+Pass+"&obs="+textoRejeitar;
                Log.i("Pika",action);
                Log.i("Pika",IDCONCRETO);
                Log.i("Pika",User);
                Log.i("Pika",Pass);
                Log.i("Pika",textoRejeitar);
            }else if(action.equals("rejeitarPedido")){
                parametros = "action="+"rejeitarPedido" +"&pedido="+IDCONCRETO+"&user_vendedor="+User+"&pass_vendedor="+Pass;
                Log.i("POKA",action);
                Log.i("POKA",IDCONCRETO);
                Log.i("POKA",User);
                Log.i("POKA",Pass);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public class solicitaDados extends AsyncTask<String, Void, String>{
        @Override
        protected String doInBackground(String... urls) {
            try {
                return Connection.postDados(urls[0], parametros);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(String resposta) {
            Log.i("REJEIUT",resposta);
            if(resposta!=null){
                if(resposta.contains("true")){
                    Toast.makeText(c, "Tudo certo!", Toast.LENGTH_SHORT).show();
                    dismiss();
                    Intent intent = new Intent(context,pedidosActivity.class);
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context,pedidosActivity.class);
                    context.startActivity(intent);
                    Toast.makeText(c, "Ops,algo deu errado :(", Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }

        }
    }
}

