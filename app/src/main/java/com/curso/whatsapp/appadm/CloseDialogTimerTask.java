/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.AlertDialog;

import java.util.TimerTask;

import android.app.AlertDialog;

import java.util.TimerTask;

/**
 * Created by lebar on 18/01/2018.
 */

public class CloseDialogTimerTask extends TimerTask {
    private AlertDialog ad;

    public CloseDialogTimerTask(AlertDialog ad){
        this.ad = ad;
    }
    @Override
    public void run (){
        if(ad.isShowing()){
            ad.dismiss();
        }
    }

}