package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private EditText usuario;
    private EditText senha;
    private Button entrar;
    private String usuarioString;
    private String senhaString;
    private String URL;
    private String parametros;
    private View view;
    private SharedPreferences.Editor editor;
    private int x = 3;
    private String userpassar;
    private String senhapassar;
    private String IDUser;
    private String NomeUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//Farm1
//Farm2
        usuario = findViewById(R.id.usuario);
        senha = findViewById(R.id.senha);
        entrar = findViewById(R.id.botentrar);
        view = findViewById(R.id.view);
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = mPreferences.edit();
        boolean verificarLogin = mPreferences.getBoolean("verificarLogin", false);
        editor = mPreferences.edit();

        if (verificarLogin == true) {
            Intent intent = new Intent(getApplicationContext(), pedidosActivity.class);
            startActivity(intent);
        }
        VerificaraConexao();
        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usuarioString = usuario.getText().toString();
                senhaString = senha.getText().toString();
                if (usuarioString.trim().equals("")) {
                    Toast.makeText(MainActivity.this, "Digite seu usuario.", Toast.LENGTH_SHORT).show();
                } else if (senhaString.trim().equals("")) {
                    Toast.makeText(MainActivity.this, "Digite sua senha.", Toast.LENGTH_SHORT).show();
                } else {
                    if (VerificaraConexao() == true) {
                        URL = "http://iffood.info/_core/_controller/vendedores.php";
                        Autenticação(usuarioString, senhaString);
                        new MainActivity.solicitaDados().execute(URL);
                    }

                }
            }
        });

    }

    public boolean VerificaraConexao() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else if (x == 3) {
            x = 0;
            Snackbar snackbar = Snackbar
                    .make(view, "Nenhuma Conexão Detectada!", Snackbar.LENGTH_LONG);

            snackbar.show();
            return false;
        } else {
            x = x + 1;
            return false;
        }
    }

    private void Autenticação(final String user, String pass) {
        parametros = "action=" + "logar" + "&user=" + user + "&pass=" + pass;
        userpassar = user;
        senhapassar = pass;
    }

    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            return Connection.postDados(urls[0], parametros);

        }

        @Override
        protected void onPostExecute(String resposta) {

            try {
                final JSONObject pedidos = new JSONObject(resposta);
                IDUser = pedidos.getString("id");
                NomeUser = pedidos.getString("nome");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            if (resposta.contains("id")) {
                Intent intent = new Intent(getApplicationContext(), pedidosActivity.class);
                startActivity(intent);
                editor.putString("User", userpassar);
                editor.putString("Pass", senhapassar);
                editor.putString("IDUSER", IDUser);
                editor.putString("NOMEUSER", NomeUser);
                editor.putBoolean("verificarLogin", true);
                editor.apply();
            } else {
                Snackbar snackbar = Snackbar
                        .make(view, "Usuario Ou Senha Incorretos", Snackbar.LENGTH_LONG);

                snackbar.show();
            }
        }
    }
}