/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ManipularDadosProduto extends AppCompatActivity {

    private SharedPreferences.Editor editor;
    private String idProduto;
    private AlertDialog alerta;
    private String nomeProduto;
    private String quantidadeDisponivel;
    private String precoProduto;
    private String imgProduto;
    private EditText nomeprodutoEdit;
    private EditText quantidadeDoProduto;
    private EditText precoDoProduto;
    private ImageView imgDoProduto;
    private EditText linkImg;
    private Button atualizainfobot;
    private String parametros;
    private String URL;
    private String User;
    private String PassUser;
    private SharedPreferences sharedPreferences;
    private String NomeUser;
    private ProgressBar progressBar;


    private String nomep;
    private String imgp;
    private String precop;
    private String quantidp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manipular_dados_produto);
        try{

        nomeprodutoEdit = findViewById(R.id.NomeProdutoManipular);
        quantidadeDoProduto = findViewById(R.id.DisponiveisManipular);
        precoDoProduto = findViewById(R.id.precoProdutoManipular);
        imgDoProduto = findViewById(R.id.editImagemProduto);
        atualizainfobot = findViewById(R.id.submeterInformacoes);
        linkImg = findViewById(R.id.linkIMG);
        progressBar = findViewById(R.id.progressManipularDados);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NomeUser = sharedPreferences.getString("NOMEUSER","Nome Não Identificado");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarManipularDados);
        toolbar.setTitle("IFFood");
        toolbar.setSubtitle(NomeUser);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = mPreferences.edit();
        User = mPreferences.getString("User","");
        PassUser = mPreferences.getString("Pass","");

        idProduto = mPreferences.getString("idProdutoManipular", String.valueOf(""));
        nomeProduto = mPreferences.getString("produto",String.valueOf(""));
        quantidadeDisponivel = mPreferences.getString("Produtosdisponiveis",String.valueOf(""));
        precoProduto = mPreferences.getString("preco",String.valueOf(""));
        imgProduto = mPreferences.getString("linkimg",String.valueOf(""));
        Log.i("MANIPULAPRODUTOS",idProduto+":"+nomeProduto+":"+quantidadeDisponivel+":"+precoProduto+":"+imgProduto);
        Log.i("IANHEWASEUSPUYTOS",precoProduto);

        LoadImage(imgProduto);
        nomeprodutoEdit.setHint(nomeProduto);
        linkImg.setHint(imgProduto);
        quantidadeDoProduto.setHint(quantidadeDisponivel);
        precoDoProduto.setHint(precoProduto);


        atualizainfobot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomep = nomeprodutoEdit.getText().toString();
                imgp = linkImg.getText().toString();
                precop = precoDoProduto.getText().toString();
                quantidp = quantidadeDoProduto.getText().toString();

                if(Conexao()==true){
                    URL = "http://iffood.info/_core/_controller/produtos.php";
                    if(nomep.isEmpty()&&precop.isEmpty()&&imgp.isEmpty()){
                        if(quantidp.isEmpty()){
                            showSnackBar(ManipularDadosProduto.this,"Altere alguma informação.");
                        }else{
                            EnviaProduto("editEstoque");
                            new ManipularDadosProduto.solicitaDados().execute(URL);
                            progress();
                            Log.i("CHEAO","CHEGA");
                        }
                    }else{
                        if(nomep.isEmpty()){
                            nomep = nomeProduto;
                        }else{
                            nomep =nomeprodutoEdit.getText().toString();
                        }
                        if(precop.isEmpty()){
                            precop = precoProduto;
                        }else{
                            precop =precoDoProduto.getText().toString();
                        }
                        if(imgp.isEmpty()){
                            imgp =imgProduto;
                        }else{
                            imgp =linkImg.getText().toString();
                        }
                        if(quantidp.isEmpty()){
                            quantidp= quantidadeDisponivel;
                        }else{
                            quantidp =quantidadeDoProduto.getText().toString();
                        }
                        EnviaProduto("editProduto");
                        new ManipularDadosProduto.solicitaDados().execute(URL);
                        progress();
                    }
                }
            }
        });
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void progress(){
        progressBar.setVisibility(View.VISIBLE);
    }
    public void tirar(){
        progressBar.setVisibility(View.GONE);
    }



    Handler h = new Handler();
    int delay = 1000; //1 second=1000 milisecond, 15*1000=15seconds
    Runnable runnable;

    @Override
    protected void onResume() {
        //start handler as activity become visible

        h.postDelayed(new Runnable() {
            public void run() {
                imgp = linkImg.getText().toString();
                if(imgp.isEmpty()){

                }else{
                    LoadImage(imgp);
                }

                runnable=this;

                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable); //stop handler when activity not visible
        super.onPause();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try{
            if(item.getItemId() == android.R.id.home){
                Intent intent = new Intent(ManipularDadosProduto.this,Manipularprodutos.class);
                startActivity(intent);
                finish();
            }
            if(item.getItemId()==R.id.RefeshMenu){
                Intent intent = new Intent(ManipularDadosProduto.this,ManipularDadosProduto.class);
                startActivity(intent);
                finish();
            }
            if(item.getItemId()==R.id.delProduto){
                //Cria o gerador do AlertDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(ManipularDadosProduto.this);
                //define o titulo
                builder.setTitle("Atenção");
                //define a mensagem
                builder.setMessage("Deseja realmente excluir esse produto?");
                //define um botão como positivo
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(Conexao()==true){
                            URL = "http://iffood.info/_core/_controller/produtos.php";
                            EnviaProduto("delProduto");
                            new ManipularDadosProduto.solicitaDados().execute(URL);
                        }else{
                            showSnackBar(ManipularDadosProduto.this,"Nenhuma Conexão Detectada!");
                        }
                    }
                });
                //define um botão como negativo.
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        alerta.dismiss();
                    }
                });
                //cria o AlertDialog
                alerta = builder.create();
                //Exibe
                alerta.show();
            }
            return super.onOptionsItemSelected(item);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_prod, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public void LoadImage(String url){
        Picasso.with(this).load(url).placeholder(R.drawable.toolbar)
                .error(R.drawable.toolbar)
                .into(imgDoProduto,new com.squareup.picasso.Callback(){

                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }
    private boolean Conexao(){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }else{
                showSnackBar(ManipularDadosProduto.this,"Nenhuma Conexão Detectada!");
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }
    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return Connection.postDados(urls[0], parametros);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(String resposta) {
            Log.i("REQ1POSTA",resposta);
            try{
                if(resposta!=null) {
                    if(resposta.equals("false")){
                        Toast.makeText(ManipularDadosProduto.this, "Algo deu errado :(", Toast.LENGTH_SHORT).show();
                    }else{
                        tirar();
                        Toast.makeText(ManipularDadosProduto.this, "Atualização efetuada com sucesso!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ManipularDadosProduto.this,Manipularprodutos.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    private void EnviaProduto(String action) {
        try{
            if(action.equals("editEstoque")){
                parametros = "action=" + action +"&disponiveis="+quantidadeDoProduto.getText().toString()+"&id="+idProduto+"&user="+User+"&pass="+PassUser;
            }
            if(action.equals("editProduto")){
                parametros = "action=" + action +"&nome="+nomep+"&preco="+precop+"&img="+imgp+"&disponiveis="+quantidp+"&id="+idProduto+"&user="+User+"&pass="+PassUser;
                Log.i("PARARPARAR",parametros);
            }
            if(action.equals("delProduto")){
                parametros = "action="+action+"&id="+idProduto+"&user="+User+"&pass="+PassUser;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
