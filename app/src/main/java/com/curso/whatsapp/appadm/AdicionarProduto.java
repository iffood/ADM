/*
 * Copyright (c)
 */

package com.curso.whatsapp.appadm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.VectorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdicionarProduto extends AppCompatActivity {
    private String URL;
    private String parametros;
    private EditText link;
    private EditText nome;
    private EditText quantidade;
    private EditText preco;
    private Button enviarProduto;
    private String link1;
    private String nome1;
    private String quantidade1;
    private String preco1;
    private SharedPreferences sharedPreferences;
    private String User;
    private String Pass;
    private ProgressBar Progress;
    private String NomeUser;
    private ImageView image;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionar_produto);

        link = findViewById(R.id.linkproduto);
        nome = findViewById(R.id.nomeprodutoEnviar);
        quantidade = findViewById(R.id.quantidadeprodutoEnviar);
        preco = findViewById(R.id.precoprodutoEnvar);
        enviarProduto = findViewById(R.id.botenviarProduto);
        Progress = findViewById(R.id.progressbaraddProduto);
        image = findViewById(R.id.ImgAdd);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NomeUser = sharedPreferences.getString("NOMEUSER","Nome Não Identificado");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarAdicionarProduto);
        toolbar.setTitle("IFFood");
        toolbar.setSubtitle(NomeUser);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        User = sharedPreferences.getString("User","");
        Pass = sharedPreferences.getString("Pass","");

        enviarProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link1=link.getText().toString();
                nome1=nome.getText().toString();
                quantidade1=quantidade.getText().toString();
                preco1=preco.getText().toString();



                if(link1.isEmpty()||nome1.isEmpty()||quantidade1.isEmpty()||preco1.isEmpty()){
                showSnackBar(AdicionarProduto.this,"Por favor,preencha os campos corretamente.");
                }else{

                Log.i("PARAPARA","entro");
                    if(Conexao()==true){
                        ProgressBar();
                        URL = "http://iffood.info/_core/_controller/produtos.php";
                        EnviaPedido("setProduto");
                        new AdicionarProduto.solicitaDados().execute(URL);
                    }

                }
            }
        });

    }


    Handler h = new Handler();
    int delay = 1000; //1 second=1000 milisecond, 15*1000=15seconds
    Runnable runnable;

    @Override
    protected void onResume() {
        //start handler as activity become visible

        h.postDelayed(new Runnable() {
            public void run() {
                link1=link.getText().toString();
                if(link1.isEmpty()){

                }else{
                    LoadImage(link1);
                }

                runnable=this;

                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    public void LoadImage(String url){
        Picasso.with(this).load(url).placeholder(R.drawable.toolbar)
                .error(R.drawable.toolbar)
                .into(image,new com.squareup.picasso.Callback(){

                    @Override
                    public void onSuccess() {
                    image.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    private void ProgressBar(){
        Progress.setVisibility(View.VISIBLE);
    }
    private void Tirar(){
        Progress.setVisibility(View.GONE);
    }

    private boolean Conexao(){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }else{
                showSnackBar(AdicionarProduto.this,"Nenhuma Conexão Detectada!");
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
        public void showSnackBar(Activity activity, String message){
        View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, 2000).show();
    }
    private class solicitaDados extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return Connection.postDados(urls[0], parametros);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
        @Override
        protected void onPostExecute(String resposta) {
            if(resposta!=null){
                Log.i("RESPOSTA111",resposta);
                if(resposta.contains("true")){
                    Tirar();
                    Intent intent = new Intent(AdicionarProduto.this,Manipularprodutos.class);
                    startActivity(intent);
                    Toast.makeText(AdicionarProduto.this, "Produto adicionado com sucesso!", Toast.LENGTH_SHORT).show();
                }else{
                    Tirar();
                    Toast.makeText(AdicionarProduto.this, "Ops,Ocorreu algum erro :(", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }
    private void EnviaPedido (String action) {
        try{
            if(action.equals("setProduto")){
                parametros = "action=" +"setProduto"+"&nome="+ nome1+"&preco="+preco1+"&img="+link1+"&disponiveis="+quantidade1+"&user="+User+"&pass="+Pass;
                Log.i("PARAPARA",parametros);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            Intent intent = new Intent(AdicionarProduto.this,Manipularprodutos.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
